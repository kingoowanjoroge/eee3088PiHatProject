# EEE3088PiHatProject

Designing a PiHat that enables Position control with feedback of a DC motor.
BILL OF MATERIALS
1. Motor Driver   = R15
2. Buck Converter = R25
3. Capacitors     = R2
4. Resistor       = R1
Total             = R43